//
//  ViewController.m
//  RuntimeTest
//
//  Created by Oleg Rahouski on 8/21/18.
//  Copyright © 2018 Oleg Rahouski. All rights reserved.
//


#import <objc/runtime.h>
#import "ViewController.h"

@interface AnotherObject : NSObject

@end

@implementation AnotherObject

-(void)selectorWithForwardingTarget {
    NSLog(@"AnotherObject - selectorWithForwardingTarget");
}

@end

@interface InvocationObject: NSObject

-(void)selectorWeCallInsteadOfUnrecognizedOne;

@end

@implementation InvocationObject

-(void)selectorWithForwardInvocation {
    NSLog(@"InvocationObject - selectorWithForwardInvocation");
}

-(void)selectorWeCallInsteadOfUnrecognizedOne {
    NSLog(@"InvocationObject - selectorWeCallInsteadOfUnrecognizedOne");
}

@end

@interface MyObject: NSObject

-(void)normalSelector;
-(void)instanceSelectorWithRuntimeAddedIMP;
+(void)classSelectorWithRuntimeAddedIMP;
-(void)selectorWithForwardingTarget;
-(void)selectorWithForwardInvocation;
-(void)selectorThatWillNotBeRecognized;

@end

@implementation MyObject

-(void)normalSelector {
    NSLog(@"normalSelector");
}

void implementation_for_instance_selectorWithRuntimeAddedIMP(id obj, SEL _cmd) {
    NSLog(@" - instance selectorWithRuntimeAddedIMP");
}

void implementation_for_class_selectorWithRuntimeAddedIMP(id obj, SEL _cmd) {
    NSLog(@" + class selectorWithRuntimeAddedIMP");
}

+(BOOL)resolveInstanceMethod:(SEL)sel {

    if (sel == @selector(instanceSelectorWithRuntimeAddedIMP)) {
        class_addMethod([self class], sel, (IMP)implementation_for_instance_selectorWithRuntimeAddedIMP, "@:@");
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}

+(BOOL)resolveClassMethod:(SEL)sel {

    if (sel == @selector(classSelectorWithRuntimeAddedIMP)) {
        Class class = object_getClass(NSClassFromString(@"MyObject"));
        class_addMethod(class, sel, (IMP)implementation_for_class_selectorWithRuntimeAddedIMP, "@:@");
    }
    return [super resolveClassMethod:sel];
}

-(id)forwardingTargetForSelector:(SEL)aSelector {
    if (aSelector == @selector(selectorWithForwardingTarget)) {
        return [[AnotherObject alloc] init];
    }
    return [super forwardingTargetForSelector:aSelector];
}

- (NSMethodSignature*)methodSignatureForSelector:(SEL)selector
{
    if (selector == @selector(selectorWithForwardInvocation)) {
        return [NSMethodSignature signatureWithObjCTypes:"v@:"];
        
        //whe can return any signature here, but not nil. if nill - we go to doesNotRecognizeSelector without forwardInvocation
        //or
        //return [[InvocationObject new] methodSignatureForSelector:@selector(selectorWeCallInsteadOfUnrecognizedOne)];
    }
    
    return [super methodSignatureForSelector:selector];;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation {

    SEL invSel = anInvocation.selector;

    AnotherObject *ao = [AnotherObject new];
    InvocationObject *io = [InvocationObject new];

    if ([ao respondsToSelector:invSel]) {
        [anInvocation invokeWithTarget:ao];
    }
    else if ([io respondsToSelector:invSel]) {
        [anInvocation invokeWithTarget:io];
    }
    else {
        [self doesNotRecognizeSelector:invSel];
    }
}

-(void)doesNotRecognizeSelector:(SEL)aSelector {

    if (aSelector == @selector(selectorThatWillNotBeRecognized)) {

        InvocationObject *io = [InvocationObject new];

        SEL altSel = @selector(selectorWeCallInsteadOfUnrecognizedOne);

        NSInvocation *invocation = [NSInvocation new];
        [invocation setSelector:altSel];
        [invocation invokeWithTarget:io];
    }
    else {
        [super doesNotRecognizeSelector:aSelector];
    }
}
@end

@interface ViewController ()
@end

@implementation ViewController

-(void)viewDidLoad {

    [super viewDidLoad];
    MyObject *m = [[MyObject alloc] init];
    [m normalSelector];
    [m instanceSelectorWithRuntimeAddedIMP];
    [MyObject classSelectorWithRuntimeAddedIMP];
    [m selectorWithForwardingTarget];
    [m selectorWithForwardInvocation];
    [m selectorThatWillNotBeRecognized];
}

@end


