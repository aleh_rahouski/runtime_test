//
//  AppDelegate.h
//  RuntimeTest
//
//  Created by Oleg Rahouski on 8/21/18.
//  Copyright © 2018 Oleg Rahouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

